import { IAccount, IScanRecord, IScanner, ISession } from "@miu-edu/types";

export const memberColumns = [
    {
        header: "ID",
        key: "id",
    },
    {
        header: "First name",
        key: "firstname",
    },
    {
        header: "Last name",
        key: "lastname",
    },
    {
        header: "Bar Code",
        key: "barCode",
    },
    {
        header: "Created on",
        key: "createdOn",
    },
];

export const eventColumns = [
    {
        header: "ID",
        key: "id",
    },
    {
        header: "Name",
        key: "name",
    },
    {
        header: "startDate",
        key: "endDate",
    },
    {
        header: "Created on",
        key: "createdOn",
    },
];
export const locationColumns = [
    {
        header: "ID",
        key: "id",
    },
    {
        header: "Name",
        key: "name",
    },
    {
        header: "Description",
        key: "description",
    },
    {
        header: "Location type",
        key: "type",
    },
    {
        header: "Created on",
        key: "createdOn",
    },
];

export const scannerColumns = [
    {
        header: "ID",
        key: "id",
    },
    {
        header: "Event",
        key: "event",
        render: (row: any) => row.event.name,
    },
    {
        header: "Account Type",
        key: "event",
        render: (row: any) => row.accountType.name,
    },
    {
        header: "Location",
        key: "event",
        render: (row: any) => row.location.name,
    },
    {
        header: "Badge",
        key: "badge",
    },
    {
        header: "Created on",
        key: "createdOn",
    },
];

export const accountTypeColumns = [
    {
        header: "ID",
        key: "id",
    },
    {
        header: "Name",
        key: "name",
    },
    {
        header: "Unit",
        key: "unit",
    },
    {
        header: "Default balance",
        key: "defaultBalance",
    },
    {
        header: "Created on",
        key: "createdOn",
    },
];

export const roleColumns = [
    {
        header: "ID",
        key: "id",
    },
    {
        header: "Name",
        key: "name",
    },

    {
        header: "Created on",
        key: "createdOn",
    },
];

export const sessionColumns = [
    {
        header: "ID",
        key: "id",
    },
    {
        header: "Session date",
        key: "sessionDate",
    },
    {
        header: "Start time",
        key: "startTime",
    },
    {
        header: "End time",
        key: "endTime",
    },

    {
        header: "Created on",
        key: "createdOn",
    },
];

export const scanRecordColumns = [
    {
        header: "ID",
        key: "id",
    },
    {
        header: "Bar code",
        key: "barCode",
    },
    {
        header: "Member",
        key: "member",
        render: (row: IScanRecord) => row.member && row.member!.firstname,
    },
    {
        header: "Scanner",
        key: "scanner",
        render: (row: IScanRecord) => row.scanner && row.scanner!.id,
    },
    {
        header: "Session",
        key: "session",
        render: (row: IScanRecord) => row.session && row.session!.id,
    },

    {
        header: "Scan date",
        key: "scanDate",
    },
];

export const accountColumns = [
    {
        header: "ID",
        key: "id",
    },
    {
        header: "Name",
        key: "name",
    },
    {
        header: "Account type",
        key: "type",
        render: (row: IAccount) => {
            return `${row.type.name}`;
        },
    },
    {
        header: "Balance",
        key: "balance",
        render: (row: IAccount) => {
            return `${row.balance} ${row.type.unit}`;
        },
    },

    {
        header: "Created on",
        key: "createdOn",
    },
];

export const attendanceColumns = [
    {
        key: "id",
        header: "ID",
    },
    {
        key: "barCode",
        header: "Bar code",
    },
    {
        key: "Member",
        header: "Member",
        render: (row: any) => {
            return row.member && `${row.member.firstname} ${row.member.lastname}`;
        },
    },
    {
        key: "location",
        header: "Location",
        render: (row: any) => {
            return `${row.scanner.location.name}: Scanner ID: ${row.scanner.id}`;
        },
    },
    {
        key: "scanDate",
        header: "Scan date",
    },
];
