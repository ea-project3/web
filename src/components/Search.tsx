import {
    Button,
    Checkbox,
    IconButton,
    Input,
    InputGroup,
    InputRightElement,
    Modal,
    ModalBody,
    ModalCloseButton,
    ModalContent,
    ModalFooter,
    ModalHeader,
    ModalOverlay,
    useDisclosure,
} from "@chakra-ui/react";
import { FC, useState } from "react";
import { BsSearch } from "react-icons/bs";
import DataTable from "./DataTable";

interface IProps {
    [key: string]: any;
}

const Search: FC<IProps> = ({ onSelect, render, multiple = true, columns, entity, ...props }) => {
    const [selected, setSelected] = useState<any>([]);
    const { isOpen, onOpen, onClose } = useDisclosure();

    const select = (row: any) => {
        if (multiple) {
            if (selected.findIndex((e: any) => e.id === row.id) >= 0)
                setSelected((s: any) => s.filter((e: any) => e.id !== row.id));
            else setSelected((s: any) => [...s, row]);
        } else setSelected(selected.length > 0 && selected.id === row.id ? [] : [row]);
    };

    const cols = [
        {
            header: "Select",
            key: "id",
            render: (row: any) => {
                return (
                    <Checkbox
                        checked={selected.findIndex((e: any) => e.id === row.id) >= 0}
                        onChange={() => select(row)}
                    />
                );
            },
        },
        ...columns,
    ];

    return (
        <>
            <InputGroup>
                <Input disabled value={render && render(selected)} />
                <InputRightElement>
                    <IconButton onClick={onOpen} aria-label="Open modal">
                        <BsSearch />
                    </IconButton>
                </InputRightElement>
            </InputGroup>
            <Modal size="6xl" isOpen={isOpen} onClose={onClose}>
                <ModalOverlay />
                <ModalContent>
                    <ModalHeader>Search</ModalHeader>
                    <ModalCloseButton />
                    <ModalBody>
                        <DataTable actions={false} columns={cols} entity={entity} queryKey={[`search-${entity}`]} />
                    </ModalBody>

                    <ModalFooter>
                        <Button
                            colorScheme="blue"
                            onClick={() => {
                                onSelect(selected);
                                onClose();
                            }}
                            size="sm"
                        >
                            Select
                        </Button>
                        <Button variant="ghost" size="sm" mr={3} onClick={onClose}>
                            Close
                        </Button>
                    </ModalFooter>
                </ModalContent>
            </Modal>
        </>
    );
};

export default Search;
