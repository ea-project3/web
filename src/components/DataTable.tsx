import { FC, useState } from "react";
import axios from "axios";

import { QueryKey, useQuery } from "@tanstack/react-query";
import {
    Box,
    Button,
    Center,
    CircularProgress,
    CircularProgressLabel,
    Menu,
    MenuButton,
    MenuItem,
    MenuList,
    Table,
    TableContainer,
    Tbody,
    Td,
    Text,
    Tfoot,
    Th,
    Thead,
    Tr,
    useToast,
} from "@chakra-ui/react";
import { IoChevronDownCircleOutline } from "react-icons/io5";
import { RiDeleteBack2Line, RiEdit2Line } from "react-icons/ri";
import { useRouter } from "next/navigation";

interface IProps {
    queryKey: QueryKey;
    entity: string;
    columns: Array<any>;
    actions?: boolean;
    additionalActions?: any;
    [key: string]: any;
}

const DataTable: FC<IProps> = ({ entity, actions = true, additionalActions, queryKey, columns, ...props }) => {
    const router = useRouter();

    const [page, setPage] = useState<number>(0);
    const [size, setSize] = useState<number>(20);
    const [total, setTotal] = useState<number>(0);
    const [totalElements, setTotalElements] = useState<number>(0);

    const queryFn = async () => {
        const response = await axios.get(`/api/${entity}`, { params: { page: page, size } });
        setTotal(response.data.totalPages);
        setTotalElements(response.data.totalElements);
        return response.data.content || response.data;
    };

    const { data, isLoading, refetch } = useQuery({
        queryFn,
        queryKey,
    });

    const toast = useToast();

    const onDelete = (id: any) => {
        console.log(id);
        const request = axios.delete(`/api/${entity}/${id}`).finally(() => refetch());

        toast.promise(request, {
            success: {
                title: "Success",
                description: `Record ${id} succesfully deleted`,
            },
            error: {
                title: "Error",
                description: `Something went wrong`,
            },
            loading: {
                title: "Deleting a record:" + id,
                description: "Please wait",
            },
        });
    };

    return (
        <TableContainer>
            <Table size="sm">
                <Thead>
                    <Tr>
                        {columns.map((column: any, index: number) => {
                            return <Th key={`header-${index}-${column.header}`}>{column.header}</Th>;
                        })}
                        {actions && <Th>Actions</Th>}
                    </Tr>
                </Thead>
                <Tbody>
                    {isLoading && (
                        <Tr>
                            <Td colSpan={columns.length}>
                                <Box
                                    w="full"
                                    h="full"
                                    flexDirection="column"
                                    display="flex"
                                    justifyContent="center"
                                    alignItems="center"
                                >
                                    <CircularProgress isIndeterminate color="green"></CircularProgress>
                                    <Text>Loading,please wait...</Text>
                                </Box>
                            </Td>
                        </Tr>
                    )}
                    {data && data.length > 0
                        ? data.map((row: any, i: number) => {
                              return (
                                  <Tr key={i}>
                                      {columns.map((col: any, k: number) => {
                                          return <Td key={k}>{col.render ? col.render(row) : row[col.key]}</Td>;
                                      })}
                                      {actions && (
                                          <Td>
                                              <Menu key={`menu-${row.id}`}>
                                                  <MenuButton
                                                      size="sm"
                                                      as={Button}
                                                      rightIcon={<IoChevronDownCircleOutline />}
                                                  >
                                                      Actions
                                                  </MenuButton>
                                                  <MenuList>
                                                      <MenuItem
                                                          onClick={() =>
                                                              router.push(`/dashboard/${entity}/details/${row.id}`)
                                                          }
                                                      >
                                                          <RiEdit2Line style={{ marginRight: 8 }} /> Edit
                                                      </MenuItem>
                                                      <MenuItem color="red" onClick={() => onDelete(row.id)}>
                                                          <RiDeleteBack2Line style={{ marginRight: 8 }} /> Delete
                                                      </MenuItem>
                                                      {additionalActions && additionalActions(row)}
                                                  </MenuList>
                                              </Menu>
                                          </Td>
                                      )}
                                  </Tr>
                              );
                          })
                        : !isLoading && (
                              <Tr>
                                  <Td colSpan={columns.length}>
                                      <Center>
                                          <Text>Empty content</Text>
                                      </Center>
                                  </Td>
                              </Tr>
                          )}
                </Tbody>
                <Tfoot>
                    <Tr>
                        <Td>Total Page: {total}</Td>

                        <Td>Total Elements: {totalElements}</Td>
                    </Tr>
                </Tfoot>
            </Table>
        </TableContainer>
    );
};

export default DataTable;
