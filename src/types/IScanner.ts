import { IAccountType, IEvent, ILocation } from "."

export default interface IScanner {
    badge?: string
    location: ILocation
    accountType: IAccountType
    id?: number | string | string[]
    event?: IEvent
    [key: string]: any
}