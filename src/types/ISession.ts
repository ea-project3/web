export default interface ISession {
    id?: number | string | string[];
    startTime: string;
    endTime: string;
    sessionDate: string;
    [key: string]: any;
}
