import { IAccountType } from ".";

export default interface IRole {
    id?: number | string | string[];
    name: string;
    defaultAccountTypes?: Array<IAccountType>;
    [key: string]: any
}
