export default interface IMember {
    id?: number | string | string[];
    firstname: string;
    lastname: string;
    barCode?: string;
    [key: string]: any;
}
