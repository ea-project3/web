export default interface IEvent {
    id?: number | string | string[];
    name: string;
    description?: string;
    startDate: string;
    endDate: string;
    startTime: string;
    endTime: string;
    scheduleType: "DAILY" | "WEEKDAYS" | "WEEKENDS";
    [key: string]: any;
}
