import { IScanner, ISession } from ".";

export default interface IScanRecord {
    id?: string | string[] | number;
    scanner?: IScanner;
    session?: ISession;
    barCode: string;

    [key: string]: any;
}
