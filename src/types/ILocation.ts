export default interface ILocation {
    name: string;
    description?: string;
    type: "GYM" | "DINING" | "LIBRARY" | "CLASSROOM" | "HALL";
    [key: string]: any
}
