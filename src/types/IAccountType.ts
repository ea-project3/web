export default interface IAccountType {
    id?: number | string | string[]
    name: string;
    description?: string;
    defaultBalance: number;
    unit: string;
    [key: string]: any;
}
