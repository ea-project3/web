import { IAccountType, IMember } from ".";

export default interface IAccount {
    id?: number | string | string[];
    description?: string;
    name: string;
    member: IMember;
    type: IAccountType;
    [key: string]: any;
}
