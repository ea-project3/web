import axios, { InternalAxiosRequestConfig } from "axios";
const instance = axios.create();

instance.interceptors.request.use((config: InternalAxiosRequestConfig) => {
    return config;
});

export default instance;
