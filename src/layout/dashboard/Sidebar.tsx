/* eslint-disable react-hooks/rules-of-hooks */
import {
    Box,
    BoxProps,
    CloseButton,
    Drawer,
    DrawerContent,
    Flex,
    FlexProps,
    Icon,
    Text,
    useColorModeValue,
    useDisclosure,
} from "@chakra-ui/react";
import { usePathname, useRouter } from "next/navigation";
import { IconType } from "react-icons";
import { BiBarcode } from "react-icons/bi";
import {
    CgCreditCard,
    CgEventbrite,
    CgHome,
    CgList,
    CgPin,
    CgScan,
    CgSmartHomeCooker,
    CgUser,
    CgUserList,
} from "react-icons/cg";
import { FiCompass, FiHome, FiSettings, FiStar, FiTrendingUp } from "react-icons/fi";
import { SlEvent } from "react-icons/sl";

interface SidebarProps extends BoxProps {
    onClose: () => void;
}

interface NavItemProps extends FlexProps {
    icon: IconType;
    href: string;
    children: React.ReactNode;
}

interface LinkItemProps {
    name: string;
    href: string;
    icon: IconType;
}

const LinkItems: Array<LinkItemProps> = [
    { name: "Events", icon: SlEvent, href: "/dashboard/events" },
    { name: "Members", icon: CgUser, href: "/dashboard/members" },
    { name: "Roles", icon: CgUserList, href: "/dashboard/roles" },
    { name: "Accounts", icon: CgCreditCard, href: "/dashboard/accounts" },
    { name: "Account types", icon: CgList, href: "/dashboard/account-types" },
    { name: "Locations", icon: CgPin, href: "/dashboard/locations" },
    { name: "Scanners", icon: CgScan, href: "/dashboard/scanners" },
    { name: "Scan barcode", icon: BiBarcode, href: "/dashboard/scan-records" },
];

const SidebarContent = ({ onClose, ...rest }: SidebarProps) => {
    return (
        <Box
            transition="3s ease"
            bg={useColorModeValue("white", "gray.900")}
            borderRight="1px"
            borderRightColor={useColorModeValue("gray.200", "gray.700")}
            w={{ base: "full", md: 60 }}
            pos="fixed"
            h="full"
            {...rest}
        >
            <Flex h="20" alignItems="center" mx="8" justifyContent="space-between">
                <Text fontSize="2xl" fontFamily="monospace" fontWeight="bold">
                    Attendance
                </Text>
                <CloseButton display={{ base: "flex", md: "none" }} onClick={onClose} />
            </Flex>
            {LinkItems.map((link) => (
                <NavItem key={link.name} href={link.href} icon={link.icon}>
                    {link.name}
                </NavItem>
            ))}
        </Box>
    );
};

const NavItem = ({ icon, href, children, ...rest }: NavItemProps) => {
    const router = useRouter();
    const pathname = usePathname();
    return (
        <Box onClick={() => router.push(href)} style={{ textDecoration: "none" }} _focus={{ boxShadow: "none" }}>
            <Flex
                align="center"
                p="2"
                mx="4"
                my="1"
                // borderRadius="lg"
                role="group"
                cursor="pointer"
                borderLeft={"3px solid transparent"}
                borderLeftColor={pathname.includes(href) ? "green.900" : "none"}
                bg={useColorModeValue(
                    pathname.includes(href) ? "gray.200" : "none",
                    pathname === href ? "black" : "none"
                )}
                color={useColorModeValue("black", pathname.includes(href) ? "primary.500" : "white")}
                _hover={{
                    bg: useColorModeValue("gray.200", pathname.includes(href) ? "black" : "none"),
                    color: useColorModeValue("black", pathname.includes(href) ? "white" : "primary.500"),
                }}
                {...rest}
            >
                {icon && (
                    <Icon
                        mr="4"
                        fontSize="16"
                        _groupHover={{
                            color: useColorModeValue("black", "green.500"),
                        }}
                        as={icon}
                    />
                )}
                {children}
            </Flex>
        </Box>
    );
};

const Sidebar = (props: SidebarProps) => {
    const { isOpen, onOpen, onClose } = useDisclosure();
    return (
        <>
            <SidebarContent onClose={() => onClose} display={{ base: "none", md: "block" }} />
            <Drawer
                isOpen={isOpen}
                placement="left"
                onClose={onClose}
                returnFocusOnClose={false}
                onOverlayClick={onClose}
                size="full"
            >
                <DrawerContent>
                    <SidebarContent onClose={onClose} />
                </DrawerContent>
            </Drawer>
        </>
    );
};

export default Sidebar;
