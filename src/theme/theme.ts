import { extendTheme } from "@chakra-ui/react";

const colors = {
    // Your primary color definition
    theme: "dark",
    primary: {
        50: "#f0f2f5",
        100: "#A6FCF7",
        300: "#8EF7EE",
        500: "#5EFC8D",
        700: "#39F7A2",
        900: "#15F257",
        // ... other shades of your primary color
    },
    // Other theme colors you want to customize (optional)
};

const theme = extendTheme({ colors });

export default theme;
