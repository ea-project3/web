import NextAuth, { SessionStrategy } from "next-auth";
import GoogleProvider from "next-auth/providers/google";
import GitHubProvider from "next-auth/providers/github";
import Auth0Provider from "next-auth/providers/auth0";
import { Provider } from "next-auth/providers/index";

const strategy: SessionStrategy = "jwt";

function SpringProvider(options: any = {}): Provider {
    return {
        id: "attendance",
        name: "Attendance Provider",
        clientId: "attendance-client",
        clientSecret: "secret",
        type: "oauth",
        version: "2.0",
        authorization: {
            params: {
                scope: "openid, profile, resources.all",
                authorizationUrl: "http://localhost:8080/attendance/oauth2/authorize",
                tokenUrl: "http://localhost:8080/attendance/oauth2/token",
                profileUrl: "http://localhost:8080/attendance/oauth2/userinfo",
                redirectUri: "http://localhost:3000/api/auth/callback/attendance",
            },
        },
        profile(p) {
            return {
                id: p.id,
            };
        },
    };
}

const authOptions = {
    providers: [
        GoogleProvider({
            clientId: process.env.GOOGLE_ID!,
            clientSecret: process.env.GOOGLE_SECRET!,
        }),
        GitHubProvider({
            clientId: process.env.GITHUB_ID!,
            clientSecret: process.env.GITHUB_SECRET!,
        }),
        // {
        //     id: "attendance",
        //     name: "Attendance",
        //     type: "oauth",
        //     version: "2.0",
        //     authorization: {
        //         params: {
        //             scope: "openid, profile, resources.all",
        //             request_uri: "http://localhost:8080/attendance/login/oauth2/code/attendance-client-oidc",
        //             redirect_uri: "http://localhost:3000/api/auth/callback/attendance",
        //         },
        //     },
        //     scope: "openid, profile, resources.all",
        //     grantType: "",
        //     clientId: "attendance-client",
        //     clientSecret: "secret",
        //     profile(p: any) {
        //         return {
        //             id: p.id,
        //             name: p.name,
        //         };
        //     },
        // },
        SpringProvider(),
    ],
    pages: {
        signIn: "/",
    },
    session: {
        strategy,
    },
};
const handler = NextAuth(authOptions);
export { handler as GET, handler as POST };
