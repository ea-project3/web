/* eslint-disable react-hooks/rules-of-hooks */
"use client";

import {
    AbsoluteCenter,
    Box,
    Button,
    Divider,
    Grid,
    GridItem,
    Heading,
    IconButton,
    Input,
    InputGroup,
    InputLeftElement,
    InputRightElement,
    Stack,
    Text,
    Tooltip,
    useColorMode,
    useColorModeValue,
    useToast,
} from "@chakra-ui/react";
import { useFormik } from "formik";
import { ILogin } from "@miu-edu/types";
import { MdEmail } from "react-icons/md";
import { CgPassword } from "react-icons/cg";
import { BsApple, BsEye, BsGithub, BsGoogle } from "react-icons/bs";
import { BiMoon, BiSun } from "react-icons/bi";
import { useRouter } from "next/navigation";
import { useSession, signIn } from "next-auth/react";
import { SiAuth0 } from "react-icons/si";

export default function Home() {
    const router = useRouter();

    const { data, status } = useSession();
    const onSubmit = (values: ILogin) => {
        // setColorMode("dark");
        router.push("/dashboard/users");
    };

    const toast = useToast();

    const { setColorMode, toggleColorMode, colorMode } = useColorMode();
    const form = useFormik({
        initialValues: {
            username: "",
            password: "",
        },
        onSubmit,
    });
    if (status === "authenticated") {
        toast({
            title: "Success",
            description: "Welcome back! " + data.user?.name,
            status: "success",
            duration: 3000,
            isClosable: true,
        });
        return router.replace("/dashboard/users");
    }
    return (
        <Grid templateColumns="repeat(12, 1fr)" gap={4} height="100vh" alignItems="center">
            <Tooltip title="Color mode">
                <IconButton
                    position="fixed"
                    top={10}
                    left={10}
                    aria-label="Theme change"
                    onClick={() => toggleColorMode()}
                >
                    {colorMode === "light" ? <BiMoon /> : <BiSun />}
                </IconButton>
            </Tooltip>
            <GridItem colSpan={[12, 6, 4]} colStart={[0, 3, 5]}>
                <form role="form" onSubmit={form.handleSubmit}>
                    <Stack>
                        <InputGroup>
                            <InputLeftElement pointerEvents="none">
                                <MdEmail />
                            </InputLeftElement>
                            <Input type="email" placeholder="E-Mail" />
                        </InputGroup>

                        <InputGroup>
                            <InputLeftElement pointerEvents="none">
                                <CgPassword />
                            </InputLeftElement>
                            <Input type="password" placeholder="Password" />
                            <InputRightElement>
                                <IconButton aria-label="Show or hide password">
                                    <BsEye />
                                </IconButton>
                            </InputRightElement>
                        </InputGroup>

                        <Button colorScheme="green" isLoading={form.isSubmitting} type="submit">
                            Login
                        </Button>
                        <Box position="relative" paddingY="6">
                            <Divider />
                            <AbsoluteCenter bg={useColorModeValue("white", "BackgroundInfo")} px="4">
                                OR
                            </AbsoluteCenter>
                        </Box>

                        <Button
                            onClick={() => signIn("google")}
                            type="button"
                            isLoading={status === "loading"}
                            leftIcon={<BsGoogle />}
                        >
                            Login with Google
                        </Button>

                        {/* 
                        
                        <Button type="button" onClick={() => signIn("github")} leftIcon={<BsGithub />}>
                            Login with GitHub
                        </Button> */}
                        <Button type="button" onClick={() => signIn("attendance")} leftIcon={<SiAuth0 />}>
                            Login with OAuth2
                        </Button>
                    </Stack>
                </form>
            </GridItem>
        </Grid>
    );
}
