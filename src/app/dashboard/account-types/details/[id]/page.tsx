"use client";
import { Box, Center, CircularProgress, CircularProgressLabel, Text } from "@chakra-ui/react";
import AccountTypeForm from "@miu-edu/forms/AccountTypeForm";
import { useQuery } from "@tanstack/react-query";
import axios from "axios";
import { useParams } from "next/navigation";

export default function AccountTypeDetailsPage() {
    const { id } = useParams();

    const queryFn = async () => {
        const response = await axios.get(`/api/account-types/${id}`);
        return response.data;
    };

    const { data, isLoading } = useQuery({
        queryFn,
        queryKey: [`account-type-details-${id}`],
        enabled: id !== "new",
    });

    return isLoading ? (
        <Box w="full" h="90vh" flexDirection="column" display="flex" justifyContent="center" alignItems="center">
            <CircularProgress isIndeterminate color="green.500"></CircularProgress>
            <Text>Loading, please wait...</Text>
        </Box>
    ) : (
        <AccountTypeForm id={id} data={data} />
    );
}
