"use client";
import { Box, Button, Card, Grid, GridItem, MenuItem, Text } from "@chakra-ui/react";
import DataTable from "@miu-edu/components/DataTable";
import { scannerColumns } from "@miu-edu/constants/columns";
import { useRouter } from "next/navigation";
import { BiPlus } from "react-icons/bi";
import { CgList, CgMore } from "react-icons/cg";

export default function ScannersPage() {
    const router = useRouter();
    return (
        <Grid gap={4}>
            <GridItem display="flex" alignItems="center" justifyContent="flex-end">
                <Button
                    size="sm"
                    leftIcon={<BiPlus />}
                    colorScheme="green"
                    onClick={() => router.push("/dashboard/scanners/details/new")}
                >
                    Create a scanner
                </Button>
            </GridItem>
            <GridItem>
                <Card p={4}>
                    <DataTable
                        additionalActions={(row: any) => {
                            return (
                                <MenuItem
                                    key="scan-records"
                                    onClick={() => router.push("/dashboard/scanners/records/" + row.id)}
                                >
                                    <CgList style={{ marginRight: 8 }} />
                                    Scan Records
                                </MenuItem>
                            );
                        }}
                        columns={scannerColumns}
                        queryKey={["scanners-list"]}
                        entity="scanners"
                    />
                </Card>
            </GridItem>
        </Grid>
    );
}
