"use client";
import { Box, Text } from "@chakra-ui/react";
import ScanRecordForm from "@miu-edu/forms/ScanRecordForm";

export default function UsersPage() {
    return (
        <Box>
            <ScanRecordForm id="new" />
        </Box>
    );
}
