"use client";
import { Box, Button, Card, Grid, GridItem, MenuItem, Text } from "@chakra-ui/react";
import DataTable from "@miu-edu/components/DataTable";
import { eventColumns } from "@miu-edu/constants/columns";
import { useRouter } from "next/navigation";
import { BiPlus } from "react-icons/bi";
import { CgList } from "react-icons/cg";

export default function AccountsPage() {
    const router = useRouter();
    return (
        <Grid gap={4}>
            <GridItem display="flex" alignItems="center" justifyContent="flex-end">
                <Button
                    size="sm"
                    leftIcon={<BiPlus />}
                    colorScheme="green"
                    onClick={() => router.push("/dashboard/events/details/new")}
                >
                    Create an event
                </Button>
            </GridItem>
            <GridItem>
                <Card p={4}>
                    <DataTable
                        additionalActions={(row: any) => {
                            return (
                                <>
                                    <MenuItem
                                        key="accounts-list"
                                        onClick={() => router.push(`/dashboard/events/${row.id}/sessions/`)}
                                    >
                                        <CgList style={{ marginRight: 8 }} />
                                        Sessions
                                    </MenuItem>
                                    <MenuItem
                                        key="accounts-list"
                                        onClick={() => router.push("/dashboard/events/attendance/" + row.id)}
                                    >
                                        <CgList style={{ marginRight: 8 }} />
                                        Attendance list
                                    </MenuItem>
                                    <MenuItem
                                        key="attendance-list"
                                        onClick={() =>
                                            router.push("/dashboard/events/attendance/" + row.id + "/member")
                                        }
                                    >
                                        <CgList style={{ marginRight: 8 }} />
                                        Attendance list by member
                                    </MenuItem>
                                </>
                            );
                        }}
                        columns={eventColumns}
                        queryKey={["events-list"]}
                        entity="events"
                    />
                </Card>
            </GridItem>
        </Grid>
    );
}
