"use client";
import { Alert, AlertIcon, Box, Button, Card, Grid, GridItem, Text } from "@chakra-ui/react";
import DataTable from "@miu-edu/components/DataTable";
import Search from "@miu-edu/components/Search";
import { accountColumns, attendanceColumns, memberColumns } from "@miu-edu/constants/columns";
import { useFormik } from "formik";
import { useParams, useRouter } from "next/navigation";

export default function ScannersRecordsPage() {
    const router = useRouter();
    const { id } = useParams();

    const onSubmit = (values: any) => {};
    const form = useFormik({
        initialValues: {
            member: null,
        },
        onSubmit,
    });
    return (
        <Card p={4}>
            <Grid gap={4}>
                {/* <GridItem display="flex" alignItems="center" justifyContent="flex-end">
                <Button
                    size="sm"
                    leftIcon={<BiPlus />}
                    colorScheme="green"
                    onClick={() => router.push("/dashboard/scanners/details/new")}
                >
                    Create a scanner
                </Button>
            </GridItem> */}
                <GridItem>
                    <form>
                        <Search
                            multiple={false}
                            onSelect={(rows: any) => {
                                console.log(rows);
                                form.setFieldValue("member", rows[0]);
                            }}
                            render={(rows: any) => {
                                return rows.map((e: any) => e.firstname);
                            }}
                            entity="members"
                            columns={memberColumns}
                        />
                    </form>
                </GridItem>
                <GridItem>
                    {form.values.member ? (
                        <DataTable
                            columns={attendanceColumns}
                            queryKey={[`event-attendance-${id}-list`]}
                            entity={`members/${form.values.member!.id!}/events/${id}/attendance`}
                        />
                    ) : (
                        <Alert status="success">
                            <AlertIcon />
                            Select a member to see attendance.
                        </Alert>
                    )}
                </GridItem>
            </Grid>
        </Card>
    );
}
