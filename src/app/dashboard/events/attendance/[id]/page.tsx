"use client";
import { Box, Button, Card, Grid, GridItem, Text } from "@chakra-ui/react";
import DataTable from "@miu-edu/components/DataTable";
import { accountColumns, attendanceColumns } from "@miu-edu/constants/columns";
import { useParams, useRouter } from "next/navigation";

export default function ScannersRecordsPage() {
    const router = useRouter();
    const { id } = useParams();
    return (
        <Grid gap={4}>
            {/* <GridItem display="flex" alignItems="center" justifyContent="flex-end">
                <Button
                    size="sm"
                    leftIcon={<BiPlus />}
                    colorScheme="green"
                    onClick={() => router.push("/dashboard/scanners/details/new")}
                >
                    Create a scanner
                </Button>
            </GridItem> */}
            <GridItem>
                <Card p={4}>
                    <DataTable
                        columns={attendanceColumns}
                        queryKey={[`event-attendance-${id}-list`]}
                        entity={`events/${id}/attendance`}
                    />
                </Card>
            </GridItem>
        </Grid>
    );
}
