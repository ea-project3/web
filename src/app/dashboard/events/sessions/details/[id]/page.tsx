"use client";
import { Box, Center, CircularProgress, CircularProgressLabel, Text } from "@chakra-ui/react";
import MemberForm from "@miu-edu/forms/MemberForm";
import { useQuery } from "@tanstack/react-query";
import axios from "axios";
import { useParams } from "next/navigation";

export default function MemberDetailsPage() {
    const { id } = useParams();

    const queryFn = async () => {
        const response = await axios.get(`/api/members/${id}`);
        return response.data;
    };

    const { data, isLoading } = useQuery({
        queryFn,
        queryKey: [`member-details-${id}`],
        enabled: id !== "new",
    });

    return isLoading ? (
        <Box w="full" h="90vh" flexDirection="column" display="flex" justifyContent="center" alignItems="center">
            <CircularProgress isIndeterminate color="green"></CircularProgress>
            <Text>Loading,please wait...</Text>
        </Box>
    ) : (
        <MemberForm id={id} data={data} />
    );
}
