"use client";
import { Box, Center, CircularProgress, CircularProgressLabel, Text } from "@chakra-ui/react";
import MemberForm from "@miu-edu/forms/MemberForm";
import SessionForm from "@miu-edu/forms/SessionForm";
import { useQuery } from "@tanstack/react-query";
import axios from "axios";
import { useParams } from "next/navigation";

export default function SessionDetailsPage() {
    const { id, sessionId } = useParams();

    const queryFn = async () => {
        const response = await axios.get(`/api/events/${id}/sessions/${sessionId}`);
        return response.data;
    };

    const { data, isLoading } = useQuery({
        queryFn,
        queryKey: [`event-session-details-${sessionId}`],
        enabled: sessionId !== "new",
    });

    return isLoading ? (
        <Box w="full" h="90vh" flexDirection="column" display="flex" justifyContent="center" alignItems="center">
            <CircularProgress isIndeterminate color="green"></CircularProgress>
            <Text>Loading,please wait...</Text>
        </Box>
    ) : (
        <SessionForm id={sessionId} eventId={id} data={data} />
    );
}
