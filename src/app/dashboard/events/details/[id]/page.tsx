"use client";
import { Box, CircularProgress, Text } from "@chakra-ui/react";
import EventForm from "@miu-edu/forms/EventForm";
import { useQuery } from "@tanstack/react-query";
import axios from "axios";
import { useParams } from "next/navigation";

export default function AccountDetailsPage() {
    const { id } = useParams();

    const queryFn = async () => {
        const response = await axios.get(`/api/events/${id}`);
        return response.data;
    };

    const { data, isLoading } = useQuery({
        queryFn,
        queryKey: [`account-details-${id}`],
        enabled: id !== "new",
    });

    return isLoading ? (
        <Box w="full" h="90vh" flexDirection="column" display="flex" justifyContent="center" alignItems="center">
            <CircularProgress isIndeterminate color="green"></CircularProgress>
            <Text>Loading,please wait...</Text>
        </Box>
    ) : (
        <EventForm id={id} data={data} />
    );
}
