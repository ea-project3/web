"use client";
import {
    Alert,
    AlertIcon,
    Box,
    Button,
    Card,
    Divider,
    FormControl,
    FormHelperText,
    FormLabel,
    Grid,
    GridItem,
    Heading,
    Input,
    Text,
} from "@chakra-ui/react";
import DataTable from "@miu-edu/components/DataTable";
import Search from "@miu-edu/components/Search";
import { accountColumns, attendanceColumns, memberColumns } from "@miu-edu/constants/columns";
import { useFormik } from "formik";
import { useParams, useRouter } from "next/navigation";
import { useState } from "react";
import { BiSearch } from "react-icons/bi";
import * as yup from "yup";

interface FormValues {
    member?: any;
    endDate: string;
    startDate: string;
    [key: string]: any;
}
export default function ScannersRecordsPage() {
    const router = useRouter();
    const { id } = useParams();

    const [filter, setFilter] = useState<any>();

    const onSubmit = (values: FormValues) => {
        setFilter(values);
    };
    const form = useFormik({
        initialValues: {
            startDate: "",
            endDate: "",
        },
        validationSchema: yup.object().shape({
            startDate: yup.string().required("Enter start date"),
            endDate: yup.string().required("Enter end date"),
        }),
        onSubmit,
    });
    return (
        <Card p={4}>
            <Grid gap={4}>
                <GridItem>
                    <Heading size="md">
                        <b>Account ID: {id}</b>: attendance list
                    </Heading>
                </GridItem>
                <Divider />
                <GridItem>
                    <form onSubmit={form.handleSubmit}>
                        <Grid gap={4} templateColumns="repeat(12, 1fr)">
                            <GridItem colSpan={[12, 3]}>
                                <FormControl isInvalid={Boolean(form.errors.startDate && form.touched.startDate)}>
                                    <FormLabel>Start date</FormLabel>
                                    <Input
                                        type="datetime-local"
                                        value={form.values.startDate}
                                        name="startDate"
                                        onChange={form.handleChange}
                                    />
                                    <FormHelperText>{form.errors.startDate}</FormHelperText>
                                </FormControl>
                            </GridItem>
                            <GridItem colSpan={[12, 3]}>
                                <FormControl isInvalid={Boolean(form.errors.endDate && form.touched.endDate)}>
                                    <FormLabel>End date</FormLabel>
                                    <Input
                                        type="datetime-local"
                                        value={form.values.endDate}
                                        name="endDate"
                                        onChange={form.handleChange}
                                    />
                                    <FormHelperText>{form.errors.endDate}</FormHelperText>
                                </FormControl>
                            </GridItem>
                            <GridItem colSpan={[12, 3]}>
                                <FormControl>
                                    <FormLabel></FormLabel>
                                    <Button type="submit" leftIcon={<BiSearch />} colorScheme="green">
                                        Search
                                    </Button>
                                </FormControl>
                            </GridItem>
                        </Grid>
                    </form>
                </GridItem>
                <GridItem>
                    {filter ? (
                        <DataTable
                            columns={attendanceColumns}
                            queryKey={[`event-attendance-${id}-list`]}
                            entity={`accounts/${id}/attendance/${filter.startDate}/${filter.endDate}`}
                        />
                    ) : (
                        <Alert status="success">
                            <AlertIcon />
                            Enter filter values to search attendance.
                        </Alert>
                    )}
                </GridItem>
            </Grid>
        </Card>
    );
}
