"use client";
import { Box, Center, CircularProgress, CircularProgressLabel, Text } from "@chakra-ui/react";
import AccountForm from "@miu-edu/forms/AccountForm";
import RoleForm from "@miu-edu/forms/RoleForm";
import { useQuery } from "@tanstack/react-query";
import axios from "axios";
import { useParams } from "next/navigation";

export default function AccountDetailsPage() {
    const { id } = useParams();

    const queryFn = async () => {
        const response = await axios.get(`/api/accounts/${id}`);
        return response.data;
    };

    const { data, isLoading } = useQuery({
        queryFn,
        queryKey: [`account-details-${id}`],
        enabled: id !== "new",
    });

    return isLoading ? (
        <Box w="full" h="90vh" flexDirection="column" display="flex" justifyContent="center" alignItems="center">
            <CircularProgress isIndeterminate color="green"></CircularProgress>
            <Text>Loading,please wait...</Text>
        </Box>
    ) : (
        <AccountForm id={id} data={data} />
    );
}
