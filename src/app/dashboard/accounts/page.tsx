"use client";
import { Box, Button, Card, Grid, GridItem, MenuItem, Text } from "@chakra-ui/react";
import DataTable from "@miu-edu/components/DataTable";
import { useRouter } from "next/navigation";
import { BiPlus } from "react-icons/bi";
import { CgList } from "react-icons/cg";

export default function AccountsPage() {
    const headers = [
        {
            header: "ID",
            key: "id",
        },
        {
            header: "Name",
            key: "name",
        },

        {
            header: "Created on",
            key: "createdOn",
        },
    ];
    const router = useRouter();
    return (
        <Grid gap={4}>
            <GridItem display="flex" alignItems="center" justifyContent="flex-end">
                <Button
                    size="sm"
                    leftIcon={<BiPlus />}
                    colorScheme="green"
                    onClick={() => router.push("/dashboard/accounts/details/new")}
                >
                    Create an account
                </Button>
            </GridItem>
            <GridItem>
                <Card p={4}>
                    <DataTable
                        additionalActions={(row: any) => {
                            return (
                                <>
                                    
                                    <MenuItem
                                        key="accounts-list"
                                        onClick={() => router.push("/dashboard/accounts/attendance/" + row.id)}
                                    >
                                        <CgList style={{ marginRight: 8 }} />
                                        Attendance list
                                    </MenuItem>
                                </>
                            );
                        }}
                        columns={headers}
                        queryKey={["accounts-list"]}
                        entity="accounts"
                    />
                </Card>
            </GridItem>
        </Grid>
    );
}
