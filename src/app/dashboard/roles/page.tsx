"use client";
import { Box, Button, Card, Grid, GridItem, Text } from "@chakra-ui/react";
import DataTable from "@miu-edu/components/DataTable";
import { useRouter } from "next/navigation";
import { BiPlus } from "react-icons/bi";

export default function UsersPage() {
    const headers = [
        {
            header: "ID",
            key: "id",
        },
        {
            header: "Name",
            key: "name",
        },

        {
            header: "Created on",
            key: "createdOn",
        },
    ];
    const router = useRouter();
    return (
        <Grid gap={4}>
            <GridItem display="flex" alignItems="center" justifyContent="flex-end">
                <Button
                    size="sm"
                    leftIcon={<BiPlus />}
                    colorScheme="green"
                    onClick={() => router.push("/dashboard/roles/details/new")}
                >
                    Create a role
                </Button>
            </GridItem>
            <GridItem>
                <Card p={4}>
                    <DataTable columns={headers} queryKey={["roles-list"]} entity="roles" />
                </Card>
            </GridItem>
        </Grid>
    );
}
