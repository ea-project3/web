"use client";
import { Box, Button, Card, Grid, GridItem, MenuItem, Text } from "@chakra-ui/react";
import DataTable from "@miu-edu/components/DataTable";
import { useRouter } from "next/navigation";
import { BiPlus } from "react-icons/bi";
import { CgList } from "react-icons/cg";

export default function UsersPage() {
    const headers = [
        {
            header: "ID",
            key: "id",
        },
        {
            header: "First name",
            key: "firstname",
        },
        {
            header: "Last name",
            key: "lastname",
        },
        {
            header: "Bar Code",
            key: "barCode",
        },
        {
            header: "Created on",
            key: "createdOn",
        },
    ];
    const router = useRouter();
    return (
        <Grid gap={4}>
            <GridItem display="flex" alignItems="center" justifyContent="flex-end">
                <Button
                    size="sm"
                    leftIcon={<BiPlus />}
                    colorScheme="green"
                    onClick={() => router.push("/dashboard/members/details/new")}
                >
                    Create a member
                </Button>
            </GridItem>
            <GridItem>
                <Card p={4}>
                    <DataTable
                        additionalActions={(row: any) => {
                            return (
                                <>
                                    <MenuItem
                                        key="accounts-list"
                                        onClick={() => router.push("/dashboard/members/roles/" + row.id)}
                                    >
                                        <CgList style={{ marginRight: 8 }} />
                                        Roles list
                                    </MenuItem>
                                    <MenuItem
                                        key="accounts-list"
                                        onClick={() => router.push("/dashboard/members/accounts/" + row.id)}
                                    >
                                        <CgList style={{ marginRight: 8 }} />
                                        Accounts list
                                    </MenuItem>
                                    <MenuItem
                                        key="accounts-list"
                                        onClick={() => router.push("/dashboard/members/attendance/" + row.id)}
                                    >
                                        <CgList style={{ marginRight: 8 }} />
                                        All attendance
                                    </MenuItem>
                                </>
                            );
                        }}
                        columns={headers}
                        queryKey={["member-lists"]}
                        entity="members"
                    />
                </Card>
            </GridItem>
        </Grid>
    );
}
