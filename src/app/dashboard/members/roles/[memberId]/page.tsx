"use client";
import { Box, Button, Card, Grid, GridItem, Heading, Text } from "@chakra-ui/react";
import DataTable from "@miu-edu/components/DataTable";
import { useParams, useRouter } from "next/navigation";
import { BiPlus } from "react-icons/bi";

export default function UsersPage() {
    const { memberId: id } = useParams();
    const headers = [
        {
            header: "ID",
            key: "id",
        },
        {
            header: "Name",
            key: "name",
        },

        {
            header: "Created on",
            key: "createdOn",
        },
    ];
    const router = useRouter();
    return (
        <Grid gap={4}>
            <GridItem display="flex" alignItems="center" justifyContent="space-between">
                <Heading size="md">
                    <b>Member ID: {id}</b> roles
                </Heading>
                <Button
                    size="sm"
                    leftIcon={<BiPlus />}
                    colorScheme="green"
                    onClick={() => router.push(`/dashboard/members/roles/${id}/details/new`)}
                >
                    Add a role
                </Button>
            </GridItem>
            <GridItem>
                <Card p={4}>
                    <DataTable columns={headers} queryKey={["roles-list"]} entity={`members/${id}/roles`} />
                </Card>
            </GridItem>
        </Grid>
    );
}
