"use client";
import { Box, Button, Card, Grid, GridItem, Text } from "@chakra-ui/react";
import DataTable from "@miu-edu/components/DataTable";
import { accountColumns, accountTypeColumns, scanRecordColumns, scannerColumns } from "@miu-edu/constants/columns";
import { useParams, useRouter } from "next/navigation";
import { BiPlus } from "react-icons/bi";

export default function ScannersRecordsPage() {
    const router = useRouter();
    const { id } = useParams();
    return (
        <Grid gap={4}>
            {/* <GridItem display="flex" alignItems="center" justifyContent="flex-end">
                <Button
                    size="sm"
                    leftIcon={<BiPlus />}
                    colorScheme="green"
                    onClick={() => router.push("/dashboard/scanners/details/new")}
                >
                    Create a scanner
                </Button>
            </GridItem> */}
            <GridItem>
                <Card p={4}>
                    <DataTable
                        columns={accountColumns}
                        queryKey={[`member-accounts-${id}-list`]}
                        entity={`members/${id}/accounts`}
                    />
                </Card>
            </GridItem>
        </Grid>
    );
}
