import { FC } from "react";
import DashboardLayout from "@miu-edu/layout/dashboard";
import { SessionProvider } from "next-auth/react";

export default function Layout({ children }: any) {
    return <DashboardLayout>{children}</DashboardLayout>;
}
