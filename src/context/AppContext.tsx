import { createContext, useState } from "react";

const AppContext = createContext({});

export const AppProvider = ({ children }: any) => {
    const [data, setData] = useState([]);
    return <AppContext.Provider value={{ data }}>{children}</AppContext.Provider>;
};

export default AppContext;
