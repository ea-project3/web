"use client";
import { createContext } from "react";

const SessionContext = createContext({});

export const SessionProvider = ({ children, ...props }: any) => {
    return <SessionContext.Provider value={{}}>{children}</SessionContext.Provider>;
};

export default SessionContext;
