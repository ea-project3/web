"use client";
import {
    Button,
    Card,
    FormControl,
    FormHelperText,
    FormLabel,
    Grid,
    GridItem,
    Heading,
    Input,
    useToast,
} from "@chakra-ui/react";
import Search from "@miu-edu/components/Search";
import { accountTypeColumns, roleColumns } from "@miu-edu/constants/columns";
import { IMember } from "@miu-edu/types";
import axios from "axios";
import { useFormik } from "formik";
import { useRouter } from "next/navigation";
import { FC, useEffect } from "react";
import { BiSend } from "react-icons/bi";
import * as yup from "yup";

interface IProps {
    id: string | string[];
    [key: string]: any;
}
const MemberRoleForm: FC<IProps> = ({ id, memberId, data, ...props }) => {
    const toast = useToast();
    const router = useRouter();
    const onSubmit = async (values: any) => {
        const request = axios.post(`/api/members/${memberId}/roles`, { ...values.role });

        toast.promise(request, {
            success: {
                title: "Success",
                description: `Succesfully added role`,
                onCloseComplete: () => router.push("/dashboard/members/roles/" + memberId),
            },

            error: {
                title: "Errror",
                description: `Something went wrong`,
            },
            loading: {
                title: "Adding a role",
                description: "Please wait",
            },
        });
    };
    const form = useFormik({
        initialValues: {
            role: null,
        },
        validationSchema: yup.object().shape({
            role: yup.mixed().required("Select role"),
        }),
        onSubmit,
    });

    useEffect(() => {
        data && form.setValues(data);
        //eslint-disable-next-line
    }, []);
    return (
        <Card p={4}>
            <form onSubmit={form.handleSubmit}>
                <Grid gap={4} templateColumns="repeat(12, 1fr)">
                    <GridItem colSpan={12}>
                        <Heading size="md">
                            <b>Member ID: {memberId}</b>
                        </Heading>
                    </GridItem>
                    <GridItem colSpan={[12]}>
                        <FormControl isInvalid={Boolean(form.errors.role && form.touched.role)}>
                            <FormLabel>Role</FormLabel>
                            <Search
                                multiple={false}
                                onSelect={(rows: any) => {
                                    console.log(rows);
                                    form.setFieldValue("role", rows[0]);
                                }}
                                render={(rows: any) => {
                                    return rows.map((e: any) => e.name);
                                }}
                                entity="roles"
                                columns={roleColumns}
                            />
                            <FormHelperText>{`${form.errors.role || ""}`}</FormHelperText>
                        </FormControl>
                    </GridItem>
                    <GridItem colSpan={12}>
                        <Button
                            leftIcon={<BiSend />}
                            type="submit"
                            colorScheme="green"
                            isLoading={form.isSubmitting}
                            disabled={!form.isValid}
                        >
                            {data ? "Edit" : "Submit"}
                        </Button>
                    </GridItem>
                </Grid>
            </form>
        </Card>
    );
};

export default MemberRoleForm;
