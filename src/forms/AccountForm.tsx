"use client";
import {
    Button,
    Card,
    FormControl,
    FormHelperText,
    FormLabel,
    Grid,
    GridItem,
    Input,
    Textarea,
    useToast,
} from "@chakra-ui/react";
import Search from "@miu-edu/components/Search";
import { accountTypeColumns, memberColumns } from "@miu-edu/constants/columns";
import { IAccount, IMember } from "@miu-edu/types";
import axios from "axios";
import { useFormik } from "formik";
import { useRouter } from "next/navigation";
import { FC, useEffect } from "react";
import { BiSend } from "react-icons/bi";
import * as yup from "yup";

interface IProps {
    id: string | string[] | number;
    [key: string]: any;
}
const AccountForm: FC<IProps> = ({ id, data, ...props }) => {
    const toast = useToast();
    const router = useRouter();
    const onSubmit = async (values: IAccount) => {
        const request =
            id === "new"
                ? axios.post("/api/accounts", { ...values, id: undefined })
                : axios.put("/api/accounts/" + id, values);
        toast.promise(request, {
            success: {
                title: "Success",
                description: `${values.name} succesfully ${data ? "edited" : "created"}`,
                onCloseComplete: () => router.push("/dashboard/accounts"),
            },

            error: {
                title: "Error",
                description: `Something went wrong`,
            },
            loading: {
                title: data ? "Editing an account" : "Creating an account",
                description: "Please wait",
            },
        });
    };
    const form = useFormik({
        initialValues: data
            ? data
            : {
                  id: id,
                  name: "",
                  description: "",
                  member: null,
                  //   unit: "",
                  //   defaultBalance: 0,
              },
        validationSchema: yup.object().shape({
            name: yup.string().required("Enter name"),
            member: yup.mixed().required("Select a member"),
            description: yup.string().required("Enter description"),
            // defaultBalance: yup.number().min(1, "Value must be greater than 1").required("Enter default balance"),
        }),
        onSubmit,
    });

    return (
        <Card p={4}>
            <form onSubmit={form.handleSubmit}>
                <Grid gap={4} templateColumns="repeat(12, 1fr)">
                    <GridItem colSpan={[12, 6]}>
                        <FormControl isInvalid={Boolean(form.errors.name && form.touched.name)}>
                            <FormLabel>Name</FormLabel>
                            <Input value={form.values.name} name="name" onChange={form.handleChange} />
                            <FormHelperText>{form.errors.name}</FormHelperText>
                        </FormControl>
                    </GridItem>
                    <GridItem colSpan={[12, 6]}>
                        <FormControl isInvalid={Boolean(form.errors.description && form.touched.description)}>
                            <FormLabel>Description</FormLabel>
                            <Input value={form.values.description} name="description" onChange={form.handleChange} />
                            <FormHelperText>{form.errors.description}</FormHelperText>
                        </FormControl>
                    </GridItem>
                    <GridItem colSpan={[12, 6]}>
                        <FormControl isInvalid={Boolean(form.errors.type && form.touched.type)}>
                            <FormLabel>Account type</FormLabel>
                            <Search
                                multiple={false}
                                onSelect={(rows: any) => {
                                    console.log(rows);
                                    form.setFieldValue("type", rows[0]);
                                }}
                                render={(rows: any) => {
                                    return rows.map((e: any) => e.name);
                                }}
                                entity="account-types"
                                columns={accountTypeColumns}
                            />
                            <FormHelperText>{`${form.errors.type || ""}`}</FormHelperText>
                        </FormControl>
                    </GridItem>
                    <GridItem colSpan={[12, 6]}>
                        <FormControl isInvalid={Boolean(form.errors.member && form.touched.member)}>
                            <FormLabel>Member</FormLabel>
                            <Search
                                multiple={false}
                                onSelect={(rows: any) => {
                                    console.log(rows);
                                    form.setFieldValue("member", rows[0]);
                                }}
                                render={(rows: any) => {
                                    return rows.map((e: any) => e.firstname);
                                }}
                                entity="members"
                                columns={memberColumns}
                            />
                            <FormHelperText>{`${form.errors.member || ""}`}</FormHelperText>
                        </FormControl>
                    </GridItem>
                    <GridItem colSpan={12}>
                        <Button
                            leftIcon={<BiSend />}
                            type="submit"
                            colorScheme="green"
                            isLoading={form.isSubmitting}
                            disabled={!form.isValid}
                        >
                            {data ? "Edit" : "Save"}
                        </Button>
                    </GridItem>
                </Grid>
            </form>
        </Card>
    );
};

export default AccountForm;
