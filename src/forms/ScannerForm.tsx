"use client";
import {
    Button,
    Card,
    FormControl,
    FormHelperText,
    FormLabel,
    Grid,
    GridItem,
    Input,
    Textarea,
    useToast,
} from "@chakra-ui/react";
import Search from "@miu-edu/components/Search";
import { accountTypeColumns, eventColumns, locationColumns, memberColumns } from "@miu-edu/constants/columns";
import { IScanner } from "@miu-edu/types";
import axios from "axios";
import { useFormik } from "formik";
import { useRouter } from "next/navigation";
import { FC, useEffect } from "react";
import { BiSend } from "react-icons/bi";
import * as yup from "yup";

interface IProps {
    id: string | string[] | number;
    [key: string]: any;
}
const ScannerForm: FC<IProps> = ({ id, data, ...props }) => {
    const toast = useToast();
    const router = useRouter();
    const onSubmit = async (values: IScanner) => {
        const request =
            id === "new"
                ? axios.post("/api/scanners", { ...values, id: undefined })
                : axios.put("/api/scanners/" + id, values);
        toast.promise(request, {
            success: {
                title: "Success",
                description: `${values.name} succesfully ${data ? "edited" : "created"}`,
                onCloseComplete: () => router.push("/dashboard/scanners"),
            },

            error: {
                title: "Error",
                description: `Something went wrong`,
            },
            loading: {
                title: data ? "Editing a scanner" : "Creating a scanner",
                description: "Please wait",
            },
        });
    };
    const form = useFormik({
        initialValues: data
            ? data
            : {
                  id: id,
                  name: "",
                  description: "",
                  location: null,
                  accountType: null,
                  event: null,
                  //   unit: "",
                  //   defaultBalance: 0,
              },
        validationSchema: yup.object().shape({
            badge: yup.string().required("Enter badge code"),
            location: yup.mixed().required("Select a location"),
            accountType: yup.mixed().required("Select an account type"),
            event: yup.mixed().required("Select an event"),
            // description: yup.string().required("Enter description"),
            // defaultBalance: yup.number().min(1, "Value must be greater than 1").required("Enter default balance"),
        }),
        onSubmit,
    });

    return (
        <Card p={4}>
            <form onSubmit={form.handleSubmit}>
                <Grid gap={4} templateColumns="repeat(12, 1fr)">
                    <GridItem colSpan={[12, 6]}>
                        <FormControl isInvalid={Boolean(form.errors.badge && form.touched.badge)}>
                            <FormLabel>Badge</FormLabel>
                            <Input value={form.values.badge} name="badge" onChange={form.handleChange} />
                            <FormHelperText>{form.errors.badge}</FormHelperText>
                        </FormControl>
                    </GridItem>
                    <GridItem colSpan={[12, 6]}>
                        <FormControl isInvalid={Boolean(form.errors.accountType && form.touched.accountType)}>
                            <FormLabel>Account type</FormLabel>
                            <Search
                                multiple={false}
                                onSelect={(rows: any) => {
                                    console.log(rows);
                                    form.setFieldValue("accountType", rows[0]);
                                }}
                                render={(rows: any) => {
                                    return rows.map((e: any) => e.name);
                                }}
                                entity="account-types"
                                columns={accountTypeColumns}
                            />
                            <FormHelperText>{`${form.errors.accountType || ""}`}</FormHelperText>
                        </FormControl>
                    </GridItem>
                    <GridItem colSpan={[12, 6]}>
                        <FormControl isInvalid={Boolean(form.errors.location && form.touched.location)}>
                            <FormLabel>Location</FormLabel>
                            <Search
                                multiple={false}
                                onSelect={(rows: any) => {
                                    console.log(rows);
                                    form.setFieldValue("location", rows[0]);
                                }}
                                render={(rows: any) => {
                                    return rows.map((e: any) => e.name);
                                }}
                                entity="locations"
                                columns={locationColumns}
                            />
                            <FormHelperText>{`${form.errors.location || ""}`}</FormHelperText>
                        </FormControl>
                    </GridItem>
                    <GridItem colSpan={[12, 6]}>
                        <FormControl isInvalid={Boolean(form.errors.event && form.touched.event)}>
                            <FormLabel>Event</FormLabel>
                            <Search
                                multiple={false}
                                onSelect={(rows: any) => {
                                    console.log(rows);
                                    form.setFieldValue("event", rows[0]);
                                }}
                                render={(rows: any) => {
                                    return rows.map((e: any) => e.name);
                                }}
                                entity="events"
                                columns={eventColumns}
                            />
                            <FormHelperText>{`${form.errors.event || ""}`}</FormHelperText>
                        </FormControl>
                    </GridItem>
                    <GridItem colSpan={12}>
                        <Button
                            leftIcon={<BiSend />}
                            type="submit"
                            colorScheme="green"
                            isLoading={form.isSubmitting}
                            disabled={!form.isValid}
                        >
                            {data ? "Edit" : "Save"}
                        </Button>
                    </GridItem>
                </Grid>
            </form>
        </Card>
    );
};

export default ScannerForm;
