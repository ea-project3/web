"use client";
import {
    Button,
    Card,
    FormControl,
    FormHelperText,
    FormLabel,
    Grid,
    GridItem,
    Input,
    Select,
    Textarea,
    useToast,
} from "@chakra-ui/react";
import Search from "@miu-edu/components/Search";
import { accountTypeColumns, memberColumns } from "@miu-edu/constants/columns";
import { IAccount, IEvent, IMember } from "@miu-edu/types";
import axios from "axios";
import { useFormik } from "formik";
import { useRouter } from "next/navigation";
import { FC, useEffect } from "react";
import { BiSend } from "react-icons/bi";
import * as yup from "yup";
import { format } from "date-fns";

interface IProps {
    id: string | string[] | number;
    [key: string]: any;
}
const EventForm: FC<IProps> = ({ id, data, ...props }) => {
    const toast = useToast();
    const router = useRouter();
    const onSubmit = async (values: IEvent) => {
        let payload: IEvent = {
            ...values,
            // startTime: format(new Date(`${values.startDate} ${values.startTime}`), "yyyy-MM-dd HH:mm:ss"),
            // endTime: format(new Date(`${values.endDate} ${values.endTime}`), "yyyy-MM-dd HH:mm:ss"),
        };

        const request =
            id === "new"
                ? axios.post("/api/events", { ...payload, id: undefined })
                : axios.put("/api/events/" + id, payload);
        toast.promise(request, {
            success: {
                title: "Success",
                description: `${values.name} succesfully ${data ? "edited" : "created"}`,
                onCloseComplete: () => router.push("/dashboard/events"),
            },

            error: {
                title: "Error",
                description: `Something went wrong`,
            },
            loading: {
                title: data ? "Editing an event" : "Creating an event",
                description: "Please wait",
            },
        });
    };
    const form = useFormik({
        initialValues: data
            ? data
            : {
                  id: id,
                  name: "",
                  description: "",
                  startDate: new Date(),
                  endDate: new Date(),
                  startTime: new Date(),
                  endTime: new Date(),
                  //   unit: "",
                  //   defaultBalance: 0,
              },
        validationSchema: yup.object().shape({
            name: yup.string().required("Enter name"),
            startDate: yup.date().required("Enter start date"),
            endDate: yup.date().required("Enter end date"),
            startTime: yup.string().required("Enter start time"),
            endTime: yup.string().required("Enter end time"),
            // member: yup.mixed().required("Select a member"),
            description: yup.string().required("Enter description"),
            // defaultBalance: yup.number().min(1, "Value must be greater than 1").required("Enter default balance"),
        }),
        onSubmit,
    });

    return (
        <Card p={4}>
            <form onSubmit={form.handleSubmit}>
                <Grid gap={4} templateColumns="repeat(12, 1fr)">
                    <GridItem colSpan={[12, 6]}>
                        <FormControl isInvalid={Boolean(form.errors.name && form.touched.name)}>
                            <FormLabel>Name</FormLabel>
                            <Input value={form.values.name} name="name" onChange={form.handleChange} />
                            <FormHelperText>{form.errors.name}</FormHelperText>
                        </FormControl>
                    </GridItem>
                    <GridItem colSpan={[12, 6]}>
                        <FormControl isInvalid={Boolean(form.errors.description && form.touched.description)}>
                            <FormLabel>Description</FormLabel>
                            <Input value={form.values.description} name="description" onChange={form.handleChange} />
                            <FormHelperText>{form.errors.description}</FormHelperText>
                        </FormControl>
                    </GridItem>
                    <GridItem colSpan={[12, 3]}>
                        <FormControl isInvalid={Boolean(form.errors.startDate && form.touched.startDate)}>
                            <FormLabel>Start date</FormLabel>
                            <Input
                                type="date"
                                value={form.values.startDate}
                                name="startDate"
                                onChange={form.handleChange}
                            />
                            <FormHelperText>{form.errors.startDate}</FormHelperText>
                        </FormControl>
                    </GridItem>
                    <GridItem colSpan={[12, 3]}>
                        <FormControl isInvalid={Boolean(form.errors.endDate && form.touched.endDate)}>
                            <FormLabel>End date</FormLabel>
                            <Input
                                type="date"
                                value={form.values.endDate}
                                name="endDate"
                                onChange={form.handleChange}
                            />
                            <FormHelperText>{form.errors.endDate}</FormHelperText>
                        </FormControl>
                    </GridItem>
                    <GridItem colSpan={[12, 3]}>
                        <FormControl isInvalid={Boolean(form.errors.startTime && form.touched.startTime)}>
                            <FormLabel>Start time</FormLabel>
                            <Input
                                type="time"
                                value={form.values.startTime}
                                name="startTime"
                                onChange={form.handleChange}
                            />
                            <FormHelperText>{form.errors.startTime}</FormHelperText>
                        </FormControl>
                    </GridItem>
                    <GridItem colSpan={[12, 3]}>
                        <FormControl isInvalid={Boolean(form.errors.endTime && form.touched.endTime)}>
                            <FormLabel>End time</FormLabel>
                            <Input
                                type="time"
                                value={form.values.endTime}
                                name="endTime"
                                onChange={form.handleChange}
                            />
                            <FormHelperText>{form.errors.endTime}</FormHelperText>
                        </FormControl>
                    </GridItem>
                    <GridItem colSpan={[12, 6]}>
                        <FormControl isInvalid={Boolean(form.errors.scheduleType && form.touched.scheduleType)}>
                            <FormLabel>Schedule type</FormLabel>
                            <Select name="scheduleType" value={form.values.scheduleType} onChange={form.handleChange}>
                                <option>-</option>
                                <option value="DAILY">Daily</option>
                                <option value="WEEKDAYS">Every weekdays</option>
                                <option value="WEEKENDS">Every weekends</option>
                            </Select>
                            <FormHelperText>{form.errors.scheduleType}</FormHelperText>
                        </FormControl>
                    </GridItem>
                    {/* <GridItem colSpan={[12, 6]}>
                        <FormControl isInvalid={Boolean(form.errors.type && form.touched.type)}>
                            <FormLabel>Account type</FormLabel>
                            <Search
                                multiple={false}
                                onSelect={(rows: any) => {
                                    console.log(rows);
                                    form.setFieldValue("type", rows[0]);
                                }}
                                render={(rows: any) => {
                                    return rows.map((e: any) => e.name);
                                }}
                                entity="account-types"
                                columns={accountTypeColumns}
                            />
                            <FormHelperText>{`${form.errors.type || ""}`}</FormHelperText>
                        </FormControl>
                    </GridItem>
                    <GridItem colSpan={[12, 6]}>
                        <FormControl isInvalid={Boolean(form.errors.member && form.touched.member)}>
                            <FormLabel>Member</FormLabel>
                            <Search
                                multiple={false}
                                onSelect={(rows: any) => {
                                    console.log(rows);
                                    form.setFieldValue("member", rows[0]);
                                }}
                                render={(rows: any) => {
                                    return rows.map((e: any) => e.firstname);
                                }}
                                entity="members"
                                columns={memberColumns}
                            />
                            <FormHelperText>{`${form.errors.member || ""}`}</FormHelperText>
                        </FormControl>
                    </GridItem> */}
                    <GridItem colSpan={12}>
                        <Button
                            leftIcon={<BiSend />}
                            type="submit"
                            colorScheme="green"
                            isLoading={form.isSubmitting}
                            disabled={!form.isValid}
                        >
                            {data ? "Edit" : "Save"}
                        </Button>
                    </GridItem>
                </Grid>
            </form>
        </Card>
    );
};

export default EventForm;
