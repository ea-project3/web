"use client";
import {
    Button,
    Card,
    FormControl,
    FormHelperText,
    FormLabel,
    Grid,
    GridItem,
    Input,
    Select,
    Textarea,
    useToast,
} from "@chakra-ui/react";
import Search from "@miu-edu/components/Search";
import { ILocation, IMember } from "@miu-edu/types";
import IRole from "@miu-edu/types/IRole";
import axios from "axios";
import { useFormik } from "formik";
import { useRouter } from "next/navigation";
import { FC, useEffect } from "react";
import { BiSend } from "react-icons/bi";
import * as yup from "yup";

interface IProps {
    id: string | string[];
    [key: string]: any;
}
const RoleForm: FC<IProps> = ({ id, data, ...props }) => {
    const toast = useToast();
    const router = useRouter();
    const onSubmit = async (values: IRole) => {
        console.log(values)
        const request =
            id === "new"
                ? axios.post("/api/roles", { ...values, id: undefined })
                : axios.put("/api/roles/" + id, values);
        toast.promise(request, {
            success: {
                title: "Success",
                description: `${values.name} succesfully ${data ? "edited" : "created"}`,
                onCloseComplete: () => router.push("/dashboard/roles"),
            },

            error: {
                title: "Error",
                description: `Something went wrong`,
            },
            loading: {
                title: data ? "Editing the role" : "Creating the role",
                description: "Please wait",
            },
        });
    };
    const form = useFormik({
        initialValues: {
            id: id,
            name: "",
            defaultAccountTypes: [],
        },
        validationSchema: yup.object().shape({
            name: yup.string().required("Enter name"),
        }),
        onSubmit,
    });

    const columns = [
        {
            header: "ID",
            key: "id",
        },
        {
            header: "Name",
            key: "name",
        },
        {
            header: "Unit",
            key: "unit",
        },
        {
            header: "Default balance",
            key: "defaultBalance",
        },
        {
            header: "Created on",
            key: "createdOn",
        },
    ];

    useEffect(() => {
        data && form.setValues(data);
    }, []);
    return (
        <Card p={4}>
            <form onSubmit={form.handleSubmit}>
                <Grid gap={4} templateColumns="repeat(12, 1fr)">
                    <GridItem colSpan={12}>
                        <FormControl isInvalid={Boolean(form.errors.name && form.touched.name)}>
                            <FormLabel>Name</FormLabel>
                            <Input name="name" value={form.values.name} onChange={form.handleChange} />
                            <FormHelperText>{form.errors.name}</FormHelperText>
                        </FormControl>
                    </GridItem>
                    <GridItem colSpan={12}>
                        <Search
                            onSelect={(rows: any) => {
                                console.log(rows)
                                form.setFieldValue("defaultAccountTypes", rows)
                            }}
                            render={(rows: any) => {
                                return rows.map((e: any) => e.name);
                            }}
                            entity="account-types"
                            columns={columns}
                        />
                    </GridItem>

                    <GridItem colSpan={12}>
                        <Button
                            leftIcon={<BiSend />}
                            type="submit"
                            colorScheme="green"
                            isLoading={form.isSubmitting}
                            disabled={!form.isValid}
                        >
                            {data ? "Edit" : "Submit"}
                        </Button>
                    </GridItem>
                </Grid>
            </form>
        </Card>
    );
};

export default RoleForm;
