"use client";
import {
    Button,
    Card,
    FormControl,
    FormHelperText,
    FormLabel,
    Grid,
    GridItem,
    Input,
    Textarea,
    useToast,
} from "@chakra-ui/react";
import Search from "@miu-edu/components/Search";
import { accountTypeColumns, memberColumns, scannerColumns, sessionColumns } from "@miu-edu/constants/columns";
import { IAccount, IMember, IScanRecord } from "@miu-edu/types";
import axios from "axios";
import { useFormik } from "formik";
import { useRouter } from "next/navigation";
import { FC, useEffect } from "react";
import { BiSend } from "react-icons/bi";
import * as yup from "yup";

interface IProps {
    id: string | string[] | number;
    [key: string]: any;
}
const ScanRecordForm: FC<IProps> = ({ id, data, ...props }) => {
    const toast = useToast();
    const router = useRouter();
    const onSubmit = async (values: IScanRecord) => {
        const request = axios.post(`/api/scanners/${values?.scanner!.id}/records`, {
            ...values,
            id: undefined,
            barCode: values.member.barCode,
        });

        toast.promise(request, {
            success: {
                title: "Success",
                description: `${values.member.firstname} succesfully scanned badge`,
                // onCloseComplete: () => router.push("/dashboard/accounts"),
            },

            error: {
                title: "Error",
                description: `Something went wrong`,
            },
            loading: {
                title: "Scanning the badge",
                description: "Please wait",
            },
        });
    };
    const form = useFormik({
        initialValues: {
            id: id,
            barCode: "",
            member: null,
            //   unit: "",
            //   defaultBalance: 0,
        },
        validationSchema: yup.object().shape({
            member: yup.mixed().required("Select a member"),
            scanner: yup.mixed().required("Select a scanner"),
            session: yup.mixed().required("Select a session"),
            // defaultBalance: yup.number().min(1, "Value must be greater than 1").required("Enter default balance"),
        }),
        onSubmit,
    });

    return (
        <Card p={4}>
            <form onSubmit={form.handleSubmit}>
                <Grid gap={4} templateColumns="repeat(12, 1fr)">
                    <GridItem colSpan={[12, 6]}>
                        <FormControl isInvalid={Boolean(form.errors.scanner && form.touched.scanner)}>
                            <FormLabel>Scanner</FormLabel>
                            <Search
                                multiple={false}
                                onSelect={(rows: any) => {
                                    console.log(rows);
                                    form.setFieldValue("scanner", rows[0]);
                                }}
                                render={(rows: any) => {
                                    return rows.map((e: any) => e.id);
                                }}
                                entity="scanners"
                                columns={scannerColumns}
                            />
                            <FormHelperText>{`${form.errors.scanner || ""}`}</FormHelperText>
                        </FormControl>
                    </GridItem>
                    <GridItem colSpan={[12, 6]}>
                        <FormControl isInvalid={Boolean(form.errors.member && form.touched.member)}>
                            <FormLabel>Member</FormLabel>
                            <Search
                                multiple={false}
                                onSelect={(rows: any) => {
                                    console.log(rows);
                                    form.setFieldValue("member", rows[0]);
                                }}
                                render={(rows: any) => {
                                    return rows.map((e: any) => e.firstname);
                                }}
                                entity="members"
                                columns={memberColumns}
                            />
                            <FormHelperText>{`${form.errors.member || ""}`}</FormHelperText>
                        </FormControl>
                    </GridItem>
                    {form.values.scanner && (
                        <GridItem colSpan={[12, 6]}>
                            <FormControl isInvalid={Boolean(form.errors.session && form.touched.session)}>
                                <FormLabel>Session</FormLabel>
                                <Search
                                    multiple={false}
                                    onSelect={(rows: any) => {
                                        console.log(rows);
                                        form.setFieldValue("session", rows[0]);
                                    }}
                                    render={(rows: any) => {
                                        return rows.map(
                                            (e: any) => `Date: ${e.sessionDate}, From: ${e.startTime} To: ${e.endTime}`
                                        );
                                    }}
                                    entity={`/events/${form.values.scanner.event?.id}/sessions`}
                                    columns={sessionColumns}
                                />
                                <FormHelperText>{`${form.errors.member || ""}`}</FormHelperText>
                            </FormControl>
                        </GridItem>
                    )}
                    <GridItem colSpan={12}>
                        <Button
                            leftIcon={<BiSend />}
                            type="submit"
                            colorScheme="green"
                            isLoading={form.isSubmitting}
                            disabled={!form.isValid}
                        >
                            Scan
                        </Button>
                    </GridItem>
                </Grid>
            </form>
        </Card>
    );
};

export default ScanRecordForm;
