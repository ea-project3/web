"use client";
import {
    Button,
    Card,
    FormControl,
    FormHelperText,
    FormLabel,
    Grid,
    GridItem,
    Input,
    Select,
    Textarea,
    useToast,
} from "@chakra-ui/react";
import Search from "@miu-edu/components/Search";
import { ILocation, IMember, ISession } from "@miu-edu/types";
import IRole from "@miu-edu/types/IRole";
import axios from "axios";
import { useFormik } from "formik";
import { useRouter } from "next/navigation";
import { FC, useEffect } from "react";
import { BiSend } from "react-icons/bi";
import * as yup from "yup";

interface IProps {
    id: string | string[];
    eventId: string | string[];
    [key: string]: any;
}
const SessionForm: FC<IProps> = ({ id, data, eventId, ...props }) => {
    const toast = useToast();
    const router = useRouter();
    const onSubmit = async (values: ISession) => {
        const request =
            id === "new"
                ? axios.post(`/api/events/${eventId}/sessions`, { ...values, id: undefined })
                : axios.put(`/api/events/${eventId}/sessions/${id}`, values);
        toast.promise(request, {
            success: {
                title: "Success",
                description: `Session succesfully ${data ? "edited" : "created"}`,
                onCloseComplete: () => router.push(`/dashboard/events/${eventId}/sessions/`),
            },

            error: {
                title: "Error",
                description: `Something went wrong`,
            },
            loading: {
                title: data ? "Editing the session" : "Creating the session",
                description: "Please wait",
            },
        });
    };
    const form = useFormik({
        initialValues: {
            id: id,
            startTime: "",
            endTime: "",
            sessionDate: "",
        },
        validationSchema: yup.object().shape({
            startTime: yup.string().required("Select start time"),
            endTime: yup.string().required("Select end time"),
            sessionDate: yup.string().required("Select session date"),
        }),
        onSubmit,
    });

    useEffect(() => {
        data && form.setValues(data);
    }, []);
    return (
        <Card p={4}>
            <form onSubmit={form.handleSubmit}>
                <Grid gap={4} templateColumns="repeat(12, 1fr)">
                    <GridItem colSpan={[12, 6]}>
                        <FormControl isInvalid={Boolean(form.errors.startTime && form.touched.startTime)}>
                            <FormLabel>Start time</FormLabel>
                            <Input
                                name="startTime"
                                type="time"
                                value={form.values.startTime}
                                onChange={form.handleChange}
                            />
                            <FormHelperText>{form.errors.startTime}</FormHelperText>
                        </FormControl>
                    </GridItem>
                    <GridItem colSpan={[12, 6]}>
                        <FormControl isInvalid={Boolean(form.errors.endTime && form.touched.endTime)}>
                            <FormLabel>End time</FormLabel>
                            <Input
                                name="endTime"
                                type="time"
                                value={form.values.endTime}
                                onChange={form.handleChange}
                            />
                            <FormHelperText>{form.errors.endTime}</FormHelperText>
                        </FormControl>
                    </GridItem>
                    <GridItem colSpan={[12, 6]}>
                        <FormControl isInvalid={Boolean(form.errors.sessionDate && form.touched.sessionDate)}>
                            <FormLabel>Session date</FormLabel>
                            <Input
                                name="sessionDate"
                                type="date"
                                value={form.values.sessionDate}
                                onChange={form.handleChange}
                            />
                            <FormHelperText>{form.errors.sessionDate}</FormHelperText>
                        </FormControl>
                    </GridItem>

                    <GridItem colSpan={12}>
                        <Button
                            leftIcon={<BiSend />}
                            type="submit"
                            colorScheme="green"
                            isLoading={form.isSubmitting}
                            disabled={!form.isValid}
                        >
                            {data ? "Edit" : "Submit"}
                        </Button>
                    </GridItem>
                </Grid>
            </form>
        </Card>
    );
};

export default SessionForm;
