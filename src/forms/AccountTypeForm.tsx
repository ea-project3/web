"use client";
import {
    Button,
    Card,
    FormControl,
    FormHelperText,
    FormLabel,
    Grid,
    GridItem,
    Input,
    Textarea,
    useToast,
} from "@chakra-ui/react";
import { IAccountType, IMember } from "@miu-edu/types";
import axios from "axios";
import { useFormik } from "formik";
import { useRouter } from "next/navigation";
import { FC, useEffect } from "react";
import { BiSend } from "react-icons/bi";
import * as yup from "yup";

interface IProps {
    id: string | string[] | number;
    [key: string]: any;
}
const AccountTypeForm: FC<IProps> = ({ id, data, ...props }) => {
    const toast = useToast();
    const router = useRouter();
    const onSubmit = async (values: IAccountType) => {
        const request =
            id === "new"
                ? axios.post("/api/account-types", { ...values, id: undefined })
                : axios.put("/api/account-types/" + id, values);
        toast.promise(request, {
            success: {
                title: "Success",
                description: `${values.name} succesfully ${data ? "edited" : "created"}`,
                onCloseComplete: () => router.push("/dashboard/account-types"),
            },

            error: {
                title: "Error",
                description: `Something went wrong`,
            },
            loading: {
                title: data ? "Editing an account type" : "Creating an account type",
                description: "Please wait",
            },
        });
    };
    const form = useFormik({
        initialValues: data
            ? data
            : {
                  id: id,
                  name: "",
                  // description: "",
                  unit: "",
                  defaultBalance: 0,
              },
        validationSchema: yup.object().shape({
            name: yup.string().required("Enter name"),
            // description: yup.string().required("Enter description"),
            unit: yup.string().required("Enter unit"),
            defaultBalance: yup.number().min(1, "Value must be greater than 1").required("Enter default balance"),
        }),
        onSubmit,
    });

    return (
        <Card p={4}>
            <form onSubmit={form.handleSubmit}>
                <Grid gap={4} templateColumns="repeat(12, 1fr)">
                    <GridItem colSpan={[12, 6, 4]}>
                        <FormControl isInvalid={Boolean(form.errors.name && form.touched.name)}>
                            <FormLabel>Name</FormLabel>
                            <Input value={form.values.name} name="name" onChange={form.handleChange} />
                            <FormHelperText>{form.errors.name}</FormHelperText>
                        </FormControl>
                    </GridItem>
                    <GridItem colSpan={[12, 6, 4]}>
                        <FormControl isInvalid={Boolean(form.errors.unit && form.touched.unit)}>
                            <FormLabel>Unit</FormLabel>
                            <Input value={form.values.unit} name="unit" onChange={form.handleChange} />
                            <FormHelperText>{form.errors.unit}</FormHelperText>
                        </FormControl>
                    </GridItem>
                    <GridItem colSpan={12}>
                        <FormControl isInvalid={Boolean(form.errors.defaultBalance && form.touched.defaultBalance)}>
                            <FormLabel>Default balance</FormLabel>
                            <Input
                                value={form.values.defaultBalance}
                                name="defaultBalance"
                                type="number"
                                onChange={form.handleChange}
                            />
                            <FormHelperText>{form.errors.defaultBalance}</FormHelperText>
                        </FormControl>
                    </GridItem>
                    <GridItem colSpan={12}>
                        <Button
                            leftIcon={<BiSend />}
                            type="submit"
                            colorScheme="green"
                            isLoading={form.isSubmitting}
                            disabled={!form.isValid}
                        >
                            {data ? "Edit" : "Save"}
                        </Button>
                    </GridItem>
                </Grid>
            </form>
        </Card>
    );
};

export default AccountTypeForm;
