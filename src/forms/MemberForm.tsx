"use client";
import {
    Button,
    Card,
    FormControl,
    FormHelperText,
    FormLabel,
    Grid,
    GridItem,
    Input,
    useToast,
} from "@chakra-ui/react";
import Search from "@miu-edu/components/Search";
import { accountTypeColumns, roleColumns } from "@miu-edu/constants/columns";
import { IMember } from "@miu-edu/types";
import axios from "axios";
import { useFormik } from "formik";
import { useRouter } from "next/navigation";
import { FC, useEffect } from "react";
import { BiSend } from "react-icons/bi";
import * as yup from "yup";

interface IProps {
    id: string | string[];
    [key: string]: any;
}
const MemberForm: FC<IProps> = ({ id, data, ...props }) => {
    const toast = useToast();
    const router = useRouter();
    const onSubmit = async (values: IMember) => {
        const request =
            id === "new"
                ? axios.post("/api/members", { ...values, id: undefined })
                : axios.put("/api/members/" + id, values);
        toast.promise(request, {
            success: {
                title: "Success",
                description: `${values.firstname} succesfully added`,
                onCloseComplete: () => router.push("/dashboard/members"),
            },

            error: {
                title: "Errror",
                description: `Something went wrong`,
            },
            loading: {
                title: "Create a member",
                description: "Please wait",
            },
        });
    };
    const form = useFormik({
        initialValues: {
            id: id,
            firstname: "",
            lastname: "",
            roles: [],
        },
        validationSchema: yup.object().shape({
            firstname: yup.string().required("Enter first name"),
            lastname: yup.string().required("Enter last name"),
            roles: yup.array().required("Select a role").min(1, "Select at least 1 role"),
        }),
        onSubmit,
    });

    useEffect(() => {
        data && form.setValues(data);
        //eslint-disable-next-line
    }, []);
    return (
        <Card p={4}>
            <form onSubmit={form.handleSubmit}>
                <Grid gap={4} templateColumns="repeat(12, 1fr)">
                    <GridItem colSpan={[12, 6]}>
                        <FormControl isInvalid={Boolean(form.errors.firstname && form.touched.firstname)}>
                            <FormLabel>First name</FormLabel>
                            <Input name="firstname" value={form.values.firstname} onChange={form.handleChange} />
                            <FormHelperText>{form.errors.firstname}</FormHelperText>
                        </FormControl>
                    </GridItem>
                    <GridItem colSpan={[12, 6]}>
                        <FormControl isInvalid={Boolean(form.errors.lastname && form.touched.lastname)}>
                            <FormLabel>Last name</FormLabel>
                            <Input name="lastname" value={form.values.lastname} onChange={form.handleChange} />
                            <FormHelperText>{form.errors.lastname}</FormHelperText>
                        </FormControl>
                    </GridItem>
                    <GridItem colSpan={[12, 6]}>
                        <FormControl isInvalid={Boolean(form.errors.barCode && form.touched.barCode)}>
                            <FormLabel>Bar code</FormLabel>
                            <Input name="barCode" value={form.values.barCode} onChange={form.handleChange} />
                            <FormHelperText>{form.errors.barCode}</FormHelperText>
                        </FormControl>
                    </GridItem>
                    <GridItem colSpan={[12]}>
                        <FormControl isInvalid={Boolean(form.errors.roles && form.touched.roles)}>
                            <FormLabel>Roles</FormLabel>
                            <Search
                                multiple={false}
                                onSelect={(rows: any) => {
                                    console.log(rows);
                                    form.setFieldValue("roles", rows);
                                }}
                                render={(rows: any) => {
                                    return rows.map((e: any) => e.name);
                                }}
                                entity="roles"
                                columns={roleColumns}
                            />
                            <FormHelperText>{`${form.errors.member || ""}`}</FormHelperText>
                        </FormControl>
                    </GridItem>
                    <GridItem colSpan={12}>
                        <Button
                            leftIcon={<BiSend />}
                            type="submit"
                            colorScheme="green"
                            isLoading={form.isSubmitting}
                            disabled={!form.isValid}
                        >
                            {data ? "Edit" : "Submit"}
                        </Button>
                    </GridItem>
                </Grid>
            </form>
        </Card>
    );
};

export default MemberForm;
