"use client";
import {
    Button,
    Card,
    FormControl,
    FormHelperText,
    FormLabel,
    Grid,
    GridItem,
    Input,
    Select,
    Textarea,
    useToast,
} from "@chakra-ui/react";
import { ILocation, IMember } from "@miu-edu/types";
import axios from "axios";
import { useFormik } from "formik";
import { useRouter } from "next/navigation";
import { FC, useEffect } from "react";
import { BiSend } from "react-icons/bi";
import * as yup from "yup";

interface IProps {
    id: string | string[];
    [key: string]: any;
}
const LocationForm: FC<IProps> = ({ id, data, ...props }) => {
    const toast = useToast();
    const router = useRouter();
    const onSubmit = async (values: ILocation) => {
        const request =
            id === "new"
                ? axios.post("/api/locations", { ...values, id: undefined })
                : axios.put("/api/locations/" + id, values);
        toast.promise(request, {
            success: {
                title: "Success",
                description: `${values.name} succesfully ${data ? "edited" : "created"}`,
                onCloseComplete: () => router.push("/dashboard/locations"),
            },

            error: {
                title: "Error",
                description: `Something went wrong`,
            },
            loading: {
                title: data ? "Editing the location" : "Creating the location",
                description: "Please wait",
            },
        });
    };
    const form = useFormik({
        initialValues: {
            id: id,
            name: "",
            type: "GYM",
        },
        validationSchema: yup.object().shape({
            name: yup.string().required("Enter name"),
            type: yup.string().required("Select type"),
        }),
        onSubmit,
    });

    useEffect(() => {
        data && form.setValues(data);
    }, []);
    return (
        <Card p={4}>
            <form onSubmit={form.handleSubmit}>
                <Grid gap={4} templateColumns="repeat(12, 1fr)">
                    <GridItem colSpan={[12, 6]}>
                        <FormControl isInvalid={Boolean(form.errors.name && form.touched.name)}>
                            <FormLabel>Name</FormLabel>
                            <Input name="name" value={form.values.name} onChange={form.handleChange} />
                            <FormHelperText>{form.errors.name}</FormHelperText>
                        </FormControl>
                    </GridItem>

                    <GridItem colSpan={[12, 6]}>
                        <FormControl isInvalid={Boolean(form.errors.type && form.touched.type)}>
                            <FormLabel>Type</FormLabel>
                            <Select name="type" value={form.values.type} onChange={form.handleChange}>
                                <option>-</option>
                                <option value="GYM">Gym</option>
                                <option value="DINING">Dining room</option>
                                <option value="LIBRARY">Library</option>
                                <option value="CLASSROOM">Classroom</option>
                                <option value="HALL">Hall</option>
                            </Select>
                            <FormHelperText>{form.errors.type}</FormHelperText>
                        </FormControl>
                    </GridItem>
                    <GridItem colSpan={12}>
                        <FormControl isInvalid={Boolean(form.errors.description && form.touched.description)}>
                            <FormLabel>Desciption</FormLabel>
                            <Textarea name="description" value={form.values.description} onChange={form.handleChange} />
                            <FormHelperText>{form.errors.description}</FormHelperText>
                        </FormControl>
                    </GridItem>
                    <GridItem colSpan={12}>
                        <Button
                            leftIcon={<BiSend />}
                            type="submit"
                            colorScheme="green"
                            isLoading={form.isSubmitting}
                            disabled={!form.isValid}
                        >
                            {data ? "Edit" : "Submit"}
                        </Button>
                    </GridItem>
                </Grid>
            </form>
        </Card>
    );
};

export default LocationForm;
