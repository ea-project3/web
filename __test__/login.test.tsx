import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import Page from "../src/app/page";
import { ThemeProvider } from "@chakra-ui/react";

jest.mock("next/navigation", () => ({
    useRouter() {
        return {
            prefetch: () => null,
        };
    },
}));
describe("Login page", () => {
    it("renders a heading", () => {
        render(
            <ThemeProvider>
                <Page />
            </ThemeProvider>
        );

        const heading = screen.getByRole("heading", { level: 6 });

        expect(heading).toBeInTheDocument();
    });

    it("renders a form", () => {
        render(
            <ThemeProvider>
                <Page />
            </ThemeProvider>
        );

        const form = screen.getByRole("form");

        expect(form).toBeInTheDocument();
    });

    it("renders a submit button", () => {
        render(<Page />);

        const button = screen.getByText("Login");

        expect(button).toBeInTheDocument();
    });

    it("renders 2 inputs", () => {
        const { container } = render(<Page />);

        const input = container.querySelectorAll("input");

        expect(input?.length).toEqual(2);
    });
});
