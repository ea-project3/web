/** @type {import('next').NextConfig} */
const nextConfig = {
    typescript: {
        // !! WARN !!
        // Dangerously allow production builds to successfully complete even if
        // your project has type errors.
        // !! WARN !!
        ignoreBuildErrors: true,
    },
    rewrites: async () => [
        {
            source: "/api/:path*",
            destination:
                process.env.NODE_ENV === "production"
                    ? "http://attendance-service:8080/attendance/:path*"
                    : "http://localhost:8080/attendance/:path*",
        },
    ],
};

export default nextConfig;
